**Вопрос 1:** Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
_Ответ:_ setTimeout() по умолчанию позволяет запланировать одноразовый вызов функции, в setInterval
() даёт возможность повторять этот вызов через установленный промежуток времени. 


**Вопрос 2:** Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
_Ответ:_ Задержка setTimeout() составляет 0мс по умолчанию. Функция сработает мгновенно... сразу после окончания предыдущей функции. 


**Вопрос 3:** Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
_Ответ:_ setInterval() держится в памяти до момента вызова clearInterval
(), его игнорирует сборщик мусора, а он тем временем, вместе со своими внешними переменными, занимает и без того ограниченное место. Как бабушки с сумками в троллейбусе в 7 утра. 