'use strict';

/* Image slider */
function imageSlideshow() {
    let imageToShow = document.querySelectorAll('.image-to-show');
    let image = 1;
    imageToShow = document.querySelectorAll('.image-to-show');
    imageToShow[image - 1].style.visibility = 'visible';

    const imageTimer = setInterval(function () {
        imageToShow[image].style.visibility = 'visible';
        if (image === 0) {
            imageToShow[imageToShow.length - 1].style.visibility = 'hidden';
            image++;
        } else if (image === imageToShow.length - 1) {
            imageToShow[image - 1].style.visibility = 'hidden';
            image = 0;
        } else {
            imageToShow[image - 1].style.visibility = 'hidden';
            image++;
        }
    }, 10000);

    const imagePause = function () {
        clearInterval(imageTimer);
    }

    const imageStart = function() {
        imageSlideshow();
    }

    document.getElementById('pause').addEventListener('click', imagePause, true);
    document.getElementById('start').addEventListener('click', imageStart, true);
}


/* Timer */
function countdown_init() {
    let countdown = 10;
    const countdownValue = document.getElementById('countdown-timer');
    const countdownTimer = setInterval(function (){
        countdown--
        countdownValue.innerHTML = countdown;
        if (countdown === 0) {
            clearInterval(countdownTimer);
            countdown_init();
        }
    }, 1000);

    const timePause = function countdownPause() {
        clearInterval(countdownTimer);
    }

    const timeStart = function countdownStart() {
        countdown_init();
    }

    document.getElementById('pause').addEventListener('click', timePause, true);
    document.getElementById('start').addEventListener('click', timeStart, true);
}